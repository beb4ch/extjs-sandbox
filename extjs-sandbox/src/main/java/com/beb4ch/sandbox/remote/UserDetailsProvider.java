package com.beb4ch.sandbox.remote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ch.ralscha.extdirectspring.annotation.ExtDirectMethod;

import com.beb4ch.sandbox.entity.Role;
import com.beb4ch.sandbox.entity.User;
import com.beb4ch.sandbox.repository.UserRepository;
import com.beb4ch.sandbox.security.JpaUserDetails;

@Component
public class UserDetailsProvider {
	
	@Autowired
	private UserRepository userRepository;
	
	@ExtDirectMethod
	@PreAuthorize("isAuthenticated()")
	@Transactional(readOnly = true)
	public  UserDetails getCurrentUser() {
		JpaUserDetails principal = (JpaUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		// Intentionally, sleep for 3 seconds, just to showcase things at the client end!
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
		}
		
		User user = userRepository.findByUserName(principal.getUsername());
		
		UserDetails userDetails = new UserDetails();
		userDetails.name = user.getName();
		userDetails.userName = user.getUserName();
		userDetails.roles = new String[ user.getRoles().size() ];
		
		int i = 0;
		for(Role role : user.getRoles())
			userDetails.roles[i++] = role.getName(); 
		
		return userDetails;
	}
	
	@SuppressWarnings("unused")
	private class UserDetails {
		public String name;
		public String userName;
		public String[] roles;
	}

}
