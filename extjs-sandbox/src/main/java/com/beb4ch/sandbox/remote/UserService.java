package com.beb4ch.sandbox.remote;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ch.ralscha.extdirectspring.annotation.ExtDirectMethod;
import ch.ralscha.extdirectspring.annotation.ExtDirectMethodType;
import ch.ralscha.extdirectspring.bean.ExtDirectStoreReadRequest;

import com.beb4ch.sandbox.entity.User;
import com.beb4ch.sandbox.remote.dto.UserDTO;
import com.beb4ch.sandbox.repository.UserRepository;

@Component
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@PreAuthorize("isAuthenticated()")
	@Transactional(readOnly = true)
	@ExtDirectMethod(ExtDirectMethodType.STORE_READ)
	public List<UserDTO> getAllUsers(ExtDirectStoreReadRequest request) {
		List<User> allUsers = userRepository.findAll();
		List<UserDTO> allUsersDTO = new ArrayList<UserDTO>(allUsers.size());
		
		for(User user : allUsers){
			UserDTO userDTO = new UserDTO();
			
			userDTO.id = user.getId();
			userDTO.email = user.getEmail();
			userDTO.name = user.getName();
			userDTO.userName = user.getUserName();
			
			allUsersDTO.add(userDTO);
		}
		
		return allUsersDTO;
	}

}
