/**
 * This package contains DTOs (Data Transfer Objects) used to send data back and forth between client and server
 * when there is no appropriate Entity class available. Usually this is the case when a single Entity class
 * contains too much or too little data.
 * 
 * In order to keep things as simple as possible and to minimize the amount of code, DTOs are not true Java Beans.
 * That is they have public properties and have no getters or setters.
 */
package com.beb4ch.sandbox.remote.dto;