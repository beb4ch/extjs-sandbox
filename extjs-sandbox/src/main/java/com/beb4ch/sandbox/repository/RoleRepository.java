package com.beb4ch.sandbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beb4ch.sandbox.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

	Role findByName(String name);

}
