package com.beb4ch.sandbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beb4ch.sandbox.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUserName(String userName);

}
