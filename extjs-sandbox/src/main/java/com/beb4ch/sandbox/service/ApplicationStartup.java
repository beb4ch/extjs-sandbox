package com.beb4ch.sandbox.service;

import org.jasypt.springsecurity3.authentication.encoding.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.beb4ch.sandbox.entity.Role;
import com.beb4ch.sandbox.entity.User;
import com.beb4ch.sandbox.repository.RoleRepository;
import com.beb4ch.sandbox.repository.UserRepository;
import com.google.common.collect.Sets;

@Component
public class ApplicationStartup implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;


	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		if(roleRepository.count() == 0) { 
			Role adminRole = new Role();
			adminRole.setName("ROLE_ADMIN");
			roleRepository.save(adminRole);
			
			Role userRole = new Role();
			userRole.setName("ROLE_USER");
			roleRepository.save(userRole);
		}
		
		if (userRepository.count() == 0) {
			//admin user
			User adminUser = new User();
			adminUser.setUserName("admin");
			adminUser.setEmail("test@beb4ch.com");
			adminUser.setName("admin");
			adminUser.setPasswordHash(passwordEncoder.encodePassword("admin", null));
			adminUser.setEnabled(true);
			adminUser.setLocale("en");

			adminUser.setRoles(Sets.newHashSet(roleRepository.findByName("ROLE_ADMIN")));

			userRepository.save(adminUser);

			//normal user
			User normalUser = new User();
			normalUser.setUserName("user");
			normalUser.setEmail("userTest@beb4ch.com");
			normalUser.setName("user");

			normalUser.setPasswordHash(passwordEncoder.encodePassword("user", null));
			normalUser.setEnabled(true);
			normalUser.setLocale("de");

			normalUser.setRoles(Sets.newHashSet(roleRepository.findByName("ROLE_USER")));

			userRepository.save(normalUser);
		}
	}


}
