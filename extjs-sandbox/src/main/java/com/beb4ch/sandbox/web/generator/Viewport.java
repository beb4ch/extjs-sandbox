package com.beb4ch.sandbox.web.generator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.beb4ch.sandbox.entity.User;
import com.beb4ch.sandbox.repository.UserRepository;
import com.beb4ch.sandbox.security.JpaUserDetails;

@Controller
public class Viewport {
	
	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value = "/js/app/view/Viewport.js", method = RequestMethod.GET)
	public ModelAndView generateViewport() {
		ModelAndView modelAndView = new ModelAndView("viewport");
		JpaUserDetails principal = (JpaUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		User user = this.userRepository.findByUserName(principal.getUsername());
		
		// To resolve lazy loaded references!
		user.toString();
		
		modelAndView.addObject("user", user);
		
		return modelAndView;
	}
	
}
