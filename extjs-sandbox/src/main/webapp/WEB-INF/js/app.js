Ext.application({
	name: 'Sandbox',
	appFolder: 'js/app',
	autoCreateViewport: true,
	
	currentUser: null,
	
	controllers: [
	   'WelcomeScreen'
    ],
	
	launch: function() {
		
		          
		if (this.hasLocalstorage()) {
			Ext.state.Manager.setProvider(Ext.create('Ext.state.LocalStorageProvider'));
		} else {
			Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));
		}

		Ext.direct.Manager.on('event', function(e) {
			if (e.code && e.code === 'parse') {
				window.location.reload();
			}
		});
		
		//ako se pojavi exception sa porukom accessdenied - pojavi se popUp prozor
		Ext.direct.Manager.on('exception', function(e) {	
			if (e.message === 'accessdenied') {
				Ext.Msg.alert(i18n.error, i18n.error_accessdenied);
			} else {
				Ext.Msg.alert(i18n.error, e.message);
			}
		});		
		
		
		Ext.apply(Ext.form.field.VTypes, {
			password: function(val, field) {
				if (field.initialPassField) {
					var pwd = field.up('form').down('#' + field.initialPassField);
					return (val == pwd.getValue());
				}
				return true;
			},

			passwordText: i18n.user_passworddonotmatch
		});

	},
	
	hasLocalstorage: function() {
		try {
			return !!localStorage.getItem;
		} catch (e) {
			return false;
		}
	},
	
	
	
});
