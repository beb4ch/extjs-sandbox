
Ext.define('Sandbox.controller.WelcomeScreen', {
	extend : 'Ext.app.Controller',
	
	refs : [ {
		ref : 'viewport',
		selector : 'viewport'
	} ],

	init : function() {
		this.control({
			'viewport' : {
				render : this.onViewportRendered
			}
		});
	},

	onViewportRendered : function() {
		userDetailsProvider.getCurrentUser(Ext.bind(this.onUserDetailsLoaded,
				this));
	},

	onUserDetailsLoaded : function(result, e) {
		// Save the current user inside the application object so we can use it whenever and wherever
		this.application.currentUser = result;
	}
});