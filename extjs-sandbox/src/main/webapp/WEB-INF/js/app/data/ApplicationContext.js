

Ext.define('Sandbox.data.ApplicationContext', {
	
	currentUser : null,
	
	getCurrentUser: function(){
		return this.currentUser;
	},
	
	setCurrentUser: function(currentUserObject){
		this.currentUser = currentUserObject;
	}
	
});