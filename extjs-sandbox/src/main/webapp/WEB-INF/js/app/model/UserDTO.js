Ext.define('Sandbox.model.UserDTO', {
	extend: 'Ext.data.Model',
	
	fields: [ 'id', 'userName', 'name', 'email' ],

	proxy: {
		type: 'direct',
		directFn: userService.getAllUsers
	}
});