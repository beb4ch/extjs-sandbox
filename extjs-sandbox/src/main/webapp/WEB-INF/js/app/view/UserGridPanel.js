Ext.define('Sandbox.view.UserGridPanel', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.userGridPanel',
	requires : 'Sandbox.model.UserDTO',

	store : {
		model : 'Sandbox.model.UserDTO',
		autoLoad : true,
	},

	columns : [ {
		text : 'ID',
		flex : 1,
		sortable : true,
		dataIndex : 'id'
	}, {
		text : 'Username',
		flex : 1,
		sortable : true,
		dataIndex : 'userName'
	}, {
		text : 'Name',
		flex : 1,
		sortable : true,
		dataIndex : 'name'
	}, {
		text : 'E-mail address',
		flex : 1,
		sortable : true,
		dataIndex : 'email'
	} ]
});