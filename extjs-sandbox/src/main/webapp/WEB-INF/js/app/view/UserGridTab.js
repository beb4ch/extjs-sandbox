Ext.define('Sandbox.view.UserGridTab', {
	extend : 'Ext.Container',
	alias : 'widget.userGridTab',

	requires : [ 'Sandbox.view.UserGridPanel' ],

	layout : {
		type : 'border',
		padding : 5
	},

	items : [ {
		region : 'center',
		xtype : 'userGridPanel'
	}, {
		region : 'south',
		html : 'Tu idu gumbići za Dodaj/Promjeni/Obriši...',
		margins : 5
	} ]

});