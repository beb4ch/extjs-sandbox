<%@ page language="java" contentType="application/x-javascript; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<spring:eval expression="${user.hasRole('ROLE_ADMIN')}" var="isAdmin"/>

Ext.define('Sandbox.view.Viewport', {
	extend: 'Ext.container.Viewport',

	layout: {
		type: 'border',
		padding: 5
	},
	
	defaults: {
		split: true
	},
	
	requires: [ 
<c:if test="${isAdmin}">	
		'Sandbox.view.UserGridTab',
</c:if>
	       ],

	items: [
	
	// NORTH REGION
		{
			region : 'north',
			xtype: 'container',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [
			    {
			    	html : 'ExtJS & Spring Sandbox',
					cls : 'appHeader',
					height : 35,
					margins : {
						top : 6,
						right : 0,
						bottom : 6,
						left : 6
					},
					flex: 1
			    },
			    {
			    	html : 'Prijavljeni ste kao ${user.name}',
					cls: 'appHeaderUsername',
					height : 35,
					margins : {
						top : 6,
						right : 0,
						bottom : 6,
						left : 6
					}
			    },
			    {
			    	html : '<a href="<spring:url value="/j_spring_security_logout"/>">Logout</a>',
			    	cls: 'appHeaderLogoutLink',
			    	height : 35,
					margins : {
						top : 6,
						right : 0,
						bottom : 6,
						left : 6
					}
			    }
			]
		},
		
	// END OF NORTH REGION
	
	// CENTER REGION

	
		{
			region : 'center',
			itemId : 'center-item',
			xtype : 'tabpanel',
			items : [
<c:if test="${isAdmin}">	
				{
					xtype: 'userGridTab',
					title: 'User administration'
				},
</c:if>
			]
		}	        
	// END OF CENTER REGION
	],


});