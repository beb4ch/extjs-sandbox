<!doctype html> 
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.Locale"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
	<link rel="shortcut icon" href="resources/favicon.ico"/> 
    <title>ExtJS + Spring Sandbox</title>
    
    <link rel="stylesheet" type="text/css" href="extjs/resources/css/ext-all.css">
    <link href='http://fonts.googleapis.com/css?family=Londrina+Sketch' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="resources/app.css"/>
    <script charset="utf-8" src="extjs/ext-all-debug.js"></script>
    
    <script src="i18n.js"></script>
    <script src="js/loader.js"></script>
    <script src="api.js"></script>
    <script src="js/direct.js"></script>
	    	    
	<script src="js/app.js"></script>
	
	<% Locale locale = RequestContextUtils.getLocale(request); %>
    <% if (locale != null) { %>
      <script src="extjs/locale/ext-lang-<%=locale.getLanguage().toLowerCase()%>.js"></script>
    <% } %>		    
	

</head>
<body>
<!--  neka promjena u index.jsp -->
</body>
</html>