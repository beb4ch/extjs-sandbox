<!doctype html> 
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.Locale"%>
<%@ page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
	<link rel="shortcut icon" href="resources/favicon.ico"/> 
    <title>ExtJS + Spring Sandbox</title>
    
    <link rel="stylesheet" type="text/css" href="extjs/resources/css/ext-all.css"/>
    <link href='http://fonts.googleapis.com/css?family=Londrina+Sketch' rel='stylesheet' type='text/css'>
    <script charset="utf-8" src="extjs/ext-all-debug.js"></script>
    
    <link rel="stylesheet" type="text/css" href="resources/app.css"/>
    <script src="i18n.js"></script>
    <script src="resources/login.js"></script>

	<% Locale locale = RequestContextUtils.getLocale(request); %>
    <% if (locale != null) { %>
      <script src="extjs/locale/ext-lang-<%=locale.getLanguage().toLowerCase()%>.js"></script>
    <% } %>		    
	<c:if test="${not empty sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message}">
	   <script type="text/javascript">
	   Ext.onReady(function() {
	     Ext.Msg.alert(i18n.error, i18n.login_failed);
	   });
	   </script>
	</c:if>
</head>
<body>
  <!--[if lt IE 9 ]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1/CFInstall.min.js"></script>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
</body>
</html>